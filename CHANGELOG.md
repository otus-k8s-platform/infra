
# Change Log
Все значимые изменения в проекте будут отраженны в этом файле

## [v0.1] - 2020.08.23

CI для terraform + GCP

### Added
- CI pipelines

### Changed
- Terraform Backend via Gitlab
- README теперь про настройку в Gitlab


## [v0.0] - 2020.08.22

Первые шаблоны terraform

### Added
- Шаблоны terraform
- README
- CHANGELOG
- LICENSE

### Changed

### Fixed
