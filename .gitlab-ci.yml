# Official image for Hashicorp's Terraform. It uses light image which is Alpine
# based as it is much lighter.
#
# Entrypoint is also needed as image by default set `terraform` binary as an
# entrypoint.
image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/google-cloud-sdk/bin'

# Default output file for Terraform plan
variables:
  GITLAB_TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}
  PLAN: plan.tfplan
  PLAN_JSON: tfplan.json
  TF_ROOT: ${CI_PROJECT_DIR}/terraform
  TF_CREDS: ${CI_PROJECT_DIR}/terraform/account.json
  GOOGLE_APPLICATION_CREDENTIALS: ${CI_PROJECT_DIR}/terraform/account.json

cache:
  paths:
    - .terraform

before_script:
  - apk --no-cache add jq
  - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
  - cd ${TF_ROOT}
  - echo $GCP_SERVICE_ACCOUNT | base64 -d > ${CI_PROJECT_DIR}/terraform/account.json
  - terraform --version
  - terraform init -backend-config="address=${GITLAB_TF_ADDRESS}" -backend-config="lock_address=${GITLAB_TF_ADDRESS}/lock" -backend-config="unlock_address=${GITLAB_TF_ADDRESS}/lock" -backend-config="username=${GITLAB_USER_LOGIN}" -backend-config="password=${GITLAB_TF_PASSWORD}" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"

stages:
  - validate
  - build
  - test
  - deploy
  - get-kubeconfig
  - destroy


validate:
  stage: validate
  script:
    - terraform validate

plan:
  stage: build
  script:
    - terraform plan -out=$PLAN
    - terraform show --json $PLAN | convert_report > $PLAN_JSON
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.tfplan
    reports:
      terraform: ${TF_ROOT}/tfplan.json

# Separate apply job for manual launching Terraform as it can be destructive
# action.
apply:
  stage: deploy
  environment:
    name: production
    on_stop: destroy
  script:
    - terraform apply -input=false $PLAN
  dependencies:
    - plan
  only:
    - master
    - tags

destroy:
  stage: destroy
  environment:
    name: production
    action: stop
  script:
    - terraform destroy --auto-approve
  when: manual
  only:
    - master
    - tags

kubeconfig:
  stage: get-kubeconfig
  environment:
    name: production
  script:
    - apk add curl bash python3 openssl
    - curl https://sdk.cloud.google.com > install.sh
    - bash install.sh --disable-prompts --install-dir=/etc/
    - echo ${GCP_SERVICE_ACCOUNT} | base64 -d > /tmp/otus-k8s-platform.json
    - gcloud auth activate-service-account --key-file=/tmp/otus-k8s-platform.json
    - gcloud config set project ${TF_VAR_project}
    - unset KUBECONFIG
    - gcloud container clusters get-credentials ${TF_VAR_cluster_name} --zone=${TF_VAR_location}
    - curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    - chmod a+x kubectl
    - mv kubectl /bin/
    - kubectl apply -f $ADMIN_POWER
    - cd ..
    - chmod a+x sa-token-kubeconfig.sh
    - ./sa-token-kubeconfig.sh config_new
    - mv config_new ~/.kube/config_new
    - curl ${CI_API_V4_URL}/groups/${CI_PROJECT_NAMESPACE}/variables/KUBECONFIG --header "PRIVATE-TOKEN:${GITLAB_TF_PASSWORD}" --request PUT --form "value=$(cat ~/.kube/config_new)" --form "variable_type=file" --form "masked=false"
