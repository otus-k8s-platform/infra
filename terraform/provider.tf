terraform {
  required_version = "~> 0.12"
}

# Google Cloud Platform
provider "google" {
  project = var.project
  region  = var.region
}
