<!--
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url] -->



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/otus-k8s-platform/docs">
    <img src="logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Инфраструктура для курсового проекта</h3>

  <p align="center">
    Шаблоны терраформа и немного магии CI
    <br />
    <a href="https://gitlab.com/otus-k8s-platform/docs"><strong>Документация »</strong></a>
    <br />
    <br />
    <a href="https://IP.xip.io/">Демо</a>
    ·
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About The Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Configuration](#configuration)
* [License](#license)


<!-- ABOUT THE PROJECT -->
## About The Project

Это часть курсового проекта по инфраструктурной платформе на базе Kubernetes


### Built With

* [Terraform](https://www.terraform.io/)
* [Google CloudPlatform](https://cloud.google.com/)
* [Gitlab CI](https://gitlab.com/)



<!-- GETTING STARTED -->
## Getting Started

Пример того как можно запустить эту часть проекта
### Prerequisites

#### Create project

```sh
export PROJECT=Project_example
export TF_CREDS=~/.config/gcloud/${PROJECT}-sa-token.json

gcloud projects create ${PROJECT} \
  --set-as-default
```

#### Create service-account and get key json

```sh
gcloud iam service-accounts create terraform \
  --display-name "Terraform admin account"

gcloud iam service-accounts keys create ${TF_CREDS} \
  --iam-account terraform@${PROJECT}.iam.gserviceaccount.com
```

#### Set roles to service-account
```sh
gcloud projects add-iam-policy-binding ${PROJECT} \
  --member serviceAccount:terraform@${PROJECT}.iam.gserviceaccount.com \
  --role roles/viewer

gcloud projects add-iam-policy-binding ${PROJECT} \
  --member serviceAccount:terraform@${PROJECT}.iam.gserviceaccount.com \
  --role roles/storage.admin
```

#### Enable services for terraform

```sh
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable serviceusage.googleapis.com
```

### Configuration

| Variable            | Description                                  | Example                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | How getting                                                                 |
|---------------------|----------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| GCP_SERVICE_ACCOUNT | Json key for google cloud provider in base64 | `{ "type" : "service_account" , "project_id" : "project" , "private_key_id" : "1231sdfasfkl12312l3klsdfasldfk12313lkmsd" , "private_key" : "-----BEGINPRIVATE######-----ENDPRIVATEKEY-----" , "client_email" : "terraform-sa@project.iam.gserviceaccount.com" , "client_id" : "123423423534531453534" , "auth_uri" : "https://accounts.google.com/o/oauth2/auth" , "token_uri" : "https://oauth2.googleapis.com/token" , "auth_provider_x509_cert_url" : "https://www.googleapis.com/oauth2/v1/certs" , "client_x509_cert_url" : "https://www.googleapis.com/robot/v1/metadata/x509/terraform-sa%40project.iam.gserviceaccount.com" }` | [Link](#create-service-account-and-get-key-json)                            |
| GITLAB_TF_PASSWORD  | Gitlab token                                 | k80idkq2v2lPMpLiNFmz                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | [Link](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) |
| GITLAB_USER_LOGIN   | Gitlab username                              | exmaple_username                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | Sign up Gitlab.com                                                          |
| TF_VAR_cluster_name | Cluster name for GKE                         | example-cluster-name                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Придумать                                                                   |
| TF_VAR_project      | Project id for GCP                           | example-project-id                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Придумать                                                                   |
| TF_VAR_location     | Location from GCP                            | europe-west4                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | [Link](https://cloud.google.com/compute/docs/regions-zones)                 |
| KUBECONFIG          | File for access to kubernetes cluster in GKE | [Link](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | CI auto add                                                                 |


<!-- ROADMAP -->
## Roadmap

Ссылка на [roadmap](https://docs.google.com/spreadsheets/d/1vLltnQkzEsrlviYZGfbiEbeYrnx4jPduGrHQi0R3zU0/edit?usp=sharing)


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=flat-square
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=flat-square
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=flat-square
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=flat-square
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
